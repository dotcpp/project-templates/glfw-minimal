# glfw-minimal

Minimal Glfw project with the latest OpenGL.

This project makes use of the following libraries/projects:

* [GLFW](https://www.glfw.org/)
* [GLM](https://github.com/g-truc/glm)
* [CPM](https://github.com/cpm-cmake/CPM.cmake)
* [GLAD](https://glad.dav1d.de/)
